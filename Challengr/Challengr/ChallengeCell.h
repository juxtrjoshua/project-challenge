//
//  ChallengeCell.h
//  Challengr
//
//  Created by Joshua Whitcombe on 17/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import "MCSwipeTableViewCell.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ChallengeCell : MCSwipeTableViewCell

@property (weak, nonatomic) IBOutlet FBSDKProfilePictureView *profile;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *taskDescription;
@property (weak, nonatomic) IBOutlet UIView *notification;
@property (weak, nonatomic) IBOutlet UIView *profileBackdrop;
@property (weak, nonatomic) IBOutlet UIImageView *promptView;

@end
