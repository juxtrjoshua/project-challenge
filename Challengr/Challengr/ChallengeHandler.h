//
//  ChallengeHandler.h
//  Challengr
//
//  Created by James Treloar on 17/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChallengerHandlerDelegate <NSObject>

-(void)recieveChallenges:(NSArray *)challenges;
@end

@interface ChallengeHandler : NSObject

@property (strong, nonatomic) id<ChallengerHandlerDelegate> delegate;

-(void)addChallengeSenderID:(NSString *)userID SenderName:(NSString *)senderName RecipientIDs:(NSArray *)recipientIDs RecipientNames:(NSArray *)recipientNames Challenge:(NSString *)challenge ;
-(void)challengeForMe:(NSString *)userID;
-(void)removeChallenge:(NSDictionary *) challenge;

@end
