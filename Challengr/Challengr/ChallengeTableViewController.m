//
//  ChallengeTableViewController.m
//  Challengr
//
//  Created by Sam on 17/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import "ChallengeTableViewController.h"
#import <MCSwipeTableViewCell/MCSwipeTableViewCell.h>
#import "ChallengeCell.h"
#import <QuartzCore/QuartzCore.h>
#import <UzysAnimatedGifPullToRefresh/UIScrollView+UzysAnimatedGifPullToRefresh.h>


@interface ChallengeTableViewController ()
@property (nonatomic, strong) NSMutableArray * challenges;
@property (nonatomic, strong) ChallengeHandler *hand;
@property (nonatomic, strong) UILabel *messageLabel;
@property(nonatomic)         SEL   action;     // default is NULL
@property(nonatomic,assign)  id    target;     // default is nil
@end

@implementation ChallengeTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //Configures the color of the top bar and adjusts translucency for best effects
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:0.2 blue:0.27 alpha:1.0];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //Hides the back button on this view in the navigation controller
    [self.navigationItem setHidesBackButton:YES];
    self.navigationController.navigationBarHidden = NO;
    
    //Configures and displays the status bar in white and ensures it is visible in this view controller
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    //Displays a rabbit at the top of the view according to the graphical guidelines
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rabbit@3.png"]];
    
    //Hides the status bar
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

    //Configures the right button of the top bar
    UIBarButtonItem *addChallengeButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"triangle@3.png"] style:UIBarButtonItemStylePlain target:self action:@selector(showSettings)];
        self.navigationItem.rightBarButtonItem = addChallengeButtonItem;
    
    //Configures the left button of the top bar
    UIBarButtonItem *settingsbuttonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"square.png"] style:UIBarButtonItemStylePlain target:self action:@selector(newChallenge)];
    self.navigationItem.leftBarButtonItem = settingsbuttonItem;
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    //An array used to store the retrieved challenges from the default server
    self.challenges = [[NSMutableArray alloc] init];
    
    //Refresh control
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshActionHandler:^{
        [weakSelf getChallenges];
    } ProgressImagesGifName:@"loader.gif" LoadingImagesGifName:@"loader.gif" ProgressScrollThreshold:1 LoadingImageFrameRate:50];
    
    // Init challenge handler
    self.hand = [[ChallengeHandler alloc] init];
    self.hand.delegate = self;
    // Get all challenges
    [self getChallenges];
    }

- (void)showSettings {
   [self performSegueWithIdentifier:@"SettingsSegue" sender:self];
}
- (void)newChallenge {
    [self performSegueWithIdentifier:@"NewChallengeSegue" sender:self];
}

- (void)getChallenges {
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 [self.hand challengeForMe:result[@"id"]];
             }
         }];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)recieveChallenges:(NSArray *)challenges {
    NSLog(@"Recieved challenges, %@", challenges);
    self.challenges = (NSMutableArray *)challenges;
    [self.tableView reloadData];
    [self.tableView performSelector:@selector(stopPullToRefreshAnimation) withObject:nil afterDelay:1.5];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (self.challenges.count > 0) {
        self.messageLabel.hidden = YES;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        return 1;
        
    } else {
        
        if (!self.messageLabel) {
            self.messageLabel = [[UILabel alloc]init];
        }
        // Display a message when the table is empty
        self.messageLabel.hidden = NO;
        
        self.messageLabel.text = @"No Challenges available, pull down to refresh";
        self.messageLabel.textColor = [UIColor blackColor];
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.textAlignment = NSTextAlignmentCenter;
        self.messageLabel.font = [UIFont fontWithName:@"Albertino" size:30];

        self.messageLabel.frame = CGRectMake(80, 0, self.view.bounds.size.width - 160, self.view.bounds.size.height);
        self.messageLabel.center = self.view.center;
        
        [self.view addSubview: self.messageLabel];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.challenges.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Configure the cell...
    static NSString *CellIdentifier = @"Cell";
    
    ChallengeCell *cell = (ChallengeCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[ChallengeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        // Remove inset of iOS 7 separators.
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            cell.separatorInset = UIEdgeInsetsZero;
        }
        
        // Setting the background color of the cell.
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    
    // Configuring the views and colors.
    UIView *checkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIColor *greenColor = [UIColor colorWithRed:132 / 255.0 green:193 / 255.0 blue:176 / 255.0 alpha:1.0];
    
    UIView *crossView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIColor *redColor = [UIColor colorWithRed:254 / 255.0 green:80 / 255.0 blue:95 / 255.0 alpha:1.0];
    
    // prompt view invisible
    cell.promptView.alpha = 0;
    // Setting the default inactive state color to the tableView background color.
    [cell setDefaultColor:[UIColor lightGrayColor]];
    //Collects and assigns the name to the listed profile
    cell.name.text = [self.challenges objectAtIndex:indexPath.row][@"senderName"];
    
    //Updates the font according to the user inteface guidelines
    cell.name.font = [UIFont fontWithName:@"Albertino" size:30.f];
    
    //Refreshes the colour of the text to the dominant colour of the associated profile image
    //cell.name.textColor = [self getDominantColor:cell.profile.image];
    
    //Sets the background of the profile to the average colour of the associated profile
    //cell.profileBackdrop.backgroundColor = [self getDominantColor:cell.profile.image];
    
    //Assigns and radially masks the profile images to conform to the prescribed graphical user interface
    cell.profileBackdrop.layer.cornerRadius = cell.profileBackdrop.bounds.size.width/2;
    cell.profileBackdrop.layer.masksToBounds = YES;
    cell.profile.layer.cornerRadius = cell.profile.bounds.size.width/2;
    cell.profile.layer.masksToBounds = YES;
    [cell.profile setProfileID:[self.challenges objectAtIndex:indexPath.row][@"senderID"]];
    
    //Assigns text to the task descrioption of the respective cell
    cell.taskDescription.text = [self.challenges objectAtIndex:indexPath.row][@"challenge"];
    
    //Changes the colour of the notification
    cell.notification.layer.cornerRadius = cell.notification.bounds.size.width/2;
    cell.notification.layer.masksToBounds = YES;
    cell.notification.backgroundColor = [UIColor colorWithRed:0.82 green:0.40 blue:0.40 alpha:1.0];
    
    [cell.notification.layer setBorderWidth:1.0];
    
    UIColor *temp = [UIColor colorWithRed:0.66 green:0.66 blue:0.66 alpha:1.0];
    [cell.notification.layer setBorderColor:temp.CGColor];
    
    // Adding gestures per state basis.
    [cell setSwipeGestureWithView:checkView color:greenColor mode:MCSwipeTableViewCellModeExit state:MCSwipeTableViewCellState1 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
        NSLog(@"Did swipe \"Checkmark\" cell");
        [tableView reloadData];
        [self performSegueWithIdentifier:@"TrackingSegue" sender:self];
    }];
    
    [cell setSwipeGestureWithView:crossView color:redColor mode:MCSwipeTableViewCellModeExit state:MCSwipeTableViewCellState3 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
        [self.hand removeChallenge:[self.challenges objectAtIndex:indexPath.row]];
        [self.challenges removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        [tableView beginUpdates];
        
        // Either delete some rows within a section (leaving at least one) or the entire section.
        if ([self.challenges count] > 0)
        {
            // Section is not yet empty, so delete only the current row.
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationTop];
        }
        else
        {
            // Section is now completely empty, so delete the entire section.
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                     withRowAnimation:UITableViewRowAnimationTop];
        }
        
        [tableView endUpdates];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ChallengeCell *cell = (ChallengeCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    [UIView animateWithDuration:0.2 animations:^{
        cell.promptView.alpha = 0.9;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.8 animations:^{
            cell.promptView.alpha = 0;
        }];
    }];
}

struct pixel {
    unsigned char r, g, b, a;
};

- (UIColor*) getDominantColor:(UIImage*)image
{
    NSUInteger red = 0;
    NSUInteger green = 0;
    NSUInteger blue = 0;
    
    
    // Allocate a buffer big enough to hold all the pixels
    
    struct pixel* pixels = (struct pixel*) calloc(1, image.size.width * image.size.height * sizeof(struct pixel));
    if (pixels != nil)
    {
        
        CGContextRef context = CGBitmapContextCreate(
                                                     (void*) pixels,
                                                     image.size.width,
                                                     image.size.height,
                                                     8,
                                                     image.size.width * 4,
                                                     CGImageGetColorSpace(image.CGImage),
                                                     kCGImageAlphaPremultipliedLast
                                                     );
        
        if (context != NULL)
        {
            // Draw the image in the bitmap
            
            CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, image.size.width, image.size.height), image.CGImage);
            
            // Now that we have the image drawn in our own buffer, we can loop over the pixels to
            // process it. This simple case simply counts all pixels that have a pure red component.
            
            // There are probably more efficient and interesting ways to do this. But the important
            // part is that the pixels buffer can be read directly.
            
            NSUInteger numberOfPixels = image.size.width * image.size.height;
            for (int i=0; i<numberOfPixels; i++) {
                red += pixels[i].r;
                green += pixels[i].g;
                blue += pixels[i].b;
            }
            
            
            red /= numberOfPixels;
            green /= numberOfPixels;
            blue/= numberOfPixels;
            
            
            CGContextRelease(context);
        }
        
        free(pixels);
    }
    return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1.0f];
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
