//
//  AddChallengeViewController.m
//  Challengr
//
//  Created by Sam on 18/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import "AddChallengeViewController.h"
#import "ChallengeHandler.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AddChallengeViewController ()
@property (nonatomic, strong) NSMutableArray *selectedFriendsArray;
@property (nonatomic, strong) NSMutableDictionary *friends;
@end

@implementation AddChallengeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[AMTagView appearance] setInnerTagColor:[UIColor colorWithRed:227.0/255.0 green:142.0/255.0 blue:150.0/255.0 alpha:1]];
    [[AMTagView appearance] setTextFont:[UIFont fontWithName:@"Albertino" size:15.f]];
    [[AMTagView appearance] setTagColor:[UIColor colorWithRed:254.0/255.0 green:80.0/255.0 blue:95.0/255.0 alpha:1]];
    [[AMTagView appearance] setHoleRadius:2];
    
    self.selectedFriendsArray = [NSMutableArray new];
    self.friends = [NSMutableDictionary new];
    
    __weak typeof(self) weakSelf = self;
    [self.friendsListView setTapHandler:^(AMTagView *view) {
            [weakSelf.selectedFriendsArray addObject:view];
            [weakSelf.friendsListView removeTag:view];
            [weakSelf.selectedFriendsListView addTagView:view];
    }];
    [self.selectedFriendsListView setTapHandler:^(AMTagView *view) {
        [view setAccessoryImage:nil];
        [weakSelf.selectedFriendsListView removeTag:view];
        [weakSelf.friendsListView addTagView:view];
    }];
    
    //Configures the left button of the top bar
    UIBarButtonItem *closebuttonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"triangle_flipped@3.png"] style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
    self.navigationItem.leftBarButtonItem = closebuttonItem;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:0.2 blue:0.27 alpha:1.0];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Albertino" size:24.f]}];
    
    [self.navigationController setToolbarHidden:NO];
    self.navigationController.toolbar.barTintColor = [UIColor colorWithRed:1 green:0.2 blue:0.27 alpha:1.0];
    self.navigationController.toolbar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *settingsbuttonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"square.png"] style:UIBarButtonItemStylePlain target:self action:@selector(addChallenge)];
    self.navigationItem.rightBarButtonItem = settingsbuttonItem;
    
    UIBarButtonItem *donebuttonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(addChallenge)];
    [self.navigationController.toolbar setItems:@[donebuttonItem]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [self getUserFriends];
    
}

- (void)dismissKeyboard {
    [self.challengeTextField resignFirstResponder];
}

- (void)addChallenge {
    if (self.selectedFriendsArray.count > 0 && self.challengeTextField.text.length > 0) {
        if ([FBSDKAccessToken currentAccessToken]) {
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (!error) {
                     [self addChallengrWithSenderId:result[@"id"] senderName:[NSString stringWithFormat:@"%@ %@", result[@"first_name"], result[@"last_name"]]];
                     [self dismissViewControllerAnimated:YES completion:nil];
                 }
             }];
        }
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"You must select friends and provide a challege description" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil] show];
    }
}

- (void)addChallengrWithSenderId:(NSString *)userId senderName:(NSString *) name{
    ChallengeHandler *hand = [ChallengeHandler new];
    NSMutableArray *recipientIds = [NSMutableArray new];
    for (AMTagView *str in self.selectedFriendsArray) {
        if (self.friends[str.tagText]) {
            [recipientIds addObject:self.friends[str.tagText][@"id"]];
        }
    }
    [hand addChallengeSenderID:userId SenderName:name RecipientIDs:recipientIds RecipientNames:recipientIds Challenge:self.challengeTextField.text];
}

- (void)getUserFriends {
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSArray *data = result[@"data"];
                 for (NSDictionary *person in data) {
                     [self.friends setObject:person forKey:person[@"name"]];
                     [self.friendsListView addTag: person[@"name"]];
                 }
             }
             else {
                 NSLog(@"%@", error.description);
             }
         }];
    }}

- (void)closeView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
