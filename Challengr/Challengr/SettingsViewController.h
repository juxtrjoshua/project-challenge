//
//  SettingsViewController.h
//  Challengr
//
//  Created by Sam on 18/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


@interface SettingsViewController : UIViewController

@end
