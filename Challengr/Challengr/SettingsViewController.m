//
//  SettingsViewController.m
//  Challengr
//
//  Created by Sam on 18/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:0.2 blue:0.27 alpha:1.0];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    //Configures the left button of the top bar
    UIBarButtonItem *closebuttonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"triangle_flipped@3.png"] style:UIBarButtonItemStylePlain target:self action:@selector(closeSettings)];
    self.navigationItem.rightBarButtonItem = closebuttonItem;
    
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.center = self.view.center;
    [self.view addSubview:loginButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeSettings {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
