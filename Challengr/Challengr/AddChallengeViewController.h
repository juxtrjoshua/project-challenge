//
//  AddChallengeViewController.h
//  Challengr
//
//  Created by Sam on 18/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AMTagListView/AMTagListView.h>

@interface AddChallengeViewController : UIViewController

@property (weak, nonatomic) IBOutlet AMTagListView *friendsListView;
@property (weak, nonatomic) IBOutlet AMTagListView *selectedFriendsListView;
@property (weak, nonatomic) IBOutlet UITextField *challengeTextField;

@end
