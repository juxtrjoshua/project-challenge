//
//  ChallengeHandler.m
//  Challengr
//
//  Created by James Treloar on 17/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import "ChallengeHandler.h"
#import <AFNetworking/AFNetworking.h>

#define IP @"10.67.0.30:1880"

@implementation ChallengeHandler

-(void)addChallengeSenderID:(NSString *)userID SenderName:(NSString *)senderName RecipientIDs:(NSArray *)recipientIDs RecipientNames:(NSArray *)recipientNames Challenge:(NSString *)challenge {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:userID,@"senderID", senderName, @"senderName", recipientIDs, @"recipientIDs", recipientNames, @"recipientNames", challenge, @"challenge",  nil];
    
    [manager POST:[NSString stringWithFormat:@"http://%@/addChallenge",IP] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error); // Bad
    }];
}

-(void)challengeForMe:(NSString *)userID {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"http://%@/getAllChallenges", IP] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *challenges = [NSMutableArray new];
        

        for (NSDictionary *challenge in responseObject) {
            
            bool forMe = false;
            for (NSString *str in challenge[@"recipientIDs"]) {
                if ([str isEqualToString:userID]) {
                    forMe = true;
                }
            }
            if (forMe) {
                [challenges addObject:(NSDictionary *)challenge];
            }
        }
        NSLog(@"%@", challenges);
        [self.delegate recieveChallenges:challenges];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    
}

-(void)removeChallenge:(NSDictionary *) challengeObject {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    [manager GET:[NSString stringWithFormat:@"http://%@/getAllChallenges", IP] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *challenges = [NSMutableArray new];
        
        for (NSDictionary *a in responseObject) {
            if (![a[@"challenge"] isEqualToString:challengeObject[@"challenge"]]) {
                [challenges addObject:a];
            }
        }
        
        [manager DELETE:[NSString stringWithFormat:@"http://%@/wipeAll", IP] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            for (NSDictionary *a in challenges) {
                
                [manager POST:[NSString stringWithFormat:@"http://%@/addChallenge",IP] parameters:a success:^(AFHTTPRequestOperation *operation, id responseObject) {
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"%@", error);
                }];
                
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Bad
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

@end





