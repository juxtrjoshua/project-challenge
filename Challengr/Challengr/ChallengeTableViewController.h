//
//  ChallengeTableViewController.h
//  Challengr
//
//  Created by Sam on 17/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChallengeHandler.h"

@interface ChallengeTableViewController : UITableViewController <UITableViewDelegate, ChallengerHandlerDelegate>

- (UIColor*) getDominantColor:(UIImage*)image;

@end
