//
//  ViewController.m
//  Challengr
//
//  Created by Sam on 15/04/2015.
//  Copyright (c) 2015 Juxtr. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <FBSDKLoginButtonDelegate>
@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Hides the navigation controller bar from this particular view
    self.navigationController.navigationBarHidden = YES;
    
    //Creates an AV player to play a video in the background of the scene
    UIView *playerView = [[UIView alloc] initWithFrame:self.view.frame];
    [playerView.layer addSublayer:self.playerLayer];
    playerView.alpha = 0.2;
    [self.view addSubview:playerView];
    [self.view sendSubviewToBack:playerView];
    
    self.loginButton.readPermissions = @[@"email", @"user_friends"];

    
    //Hides the status bar
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    //Facebook references and generates an access token for the segue (if logged in) and a token for the button if not logged in
    if ([FBSDKAccessToken currentAccessToken]) {
        [self performSegueWithIdentifier:@"ChallengeSegue" sender:self];
    }
    
    else {
        //Delegates the access token button with the respective token
        self.loginButton.delegate = self;
    }
}

//Loads up the login button for facebook, this will accomodate for customizability of buttons within this particular view
- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {

    if (!result.isCancelled) {
        [self performSegueWithIdentifier:@"ChallengeSegue" sender:self];
    }
    
}

//Resets the login button if you wish to logout from the view
- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
  
    //Provides an alert view if not logged into the application via Facebook
    UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Login Error" message:@"You must login to facebook to continue" delegate:self cancelButtonTitle:@"Got it" otherButtonTitles: nil];
    [errorAlertView show];
    
}

//Pushes through to the primary view, the "ChallengeSegue" view
- (void)showChallengeView {
    
    //Performs the segue specified for the Challenge Segue
    [self performSegueWithIdentifier:@"ChallengeSegue" sender:self];
    
}

-(AVPlayerLayer*)playerLayer{
    
    ///Finds the position of the movie within the references folder
    NSString *moviePath = [[NSBundle mainBundle]
                           pathForResource:@"backdrop" ofType:@"mp4"];
    NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
    
    //Generates and positions the player within the sub view
    _playerLayer = [AVPlayerLayer playerLayerWithPlayer:
                    [[AVPlayer alloc]initWithURL:movieURL]];
    _playerLayer.frame = CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height);
    _playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [_playerLayer.player play];
    
    //Mutes the volume of the video so as not to distract the user
    _playerLayer.player.volume = 0;
    
    return _playerLayer;
    
}

@end
